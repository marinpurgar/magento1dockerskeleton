#!/usr/bin/env bash

cd /tmp

tar xf sample-data-1.9.1.0.tar.gz

cp -R magento-sample-data-1.9.1.0/media/* /var/www/html/media/
cp -R magento-sample-data-1.9.1.0/skin/* /var/www/html/skin/

chown -R www-data:www-data /var/www/html/media /var/www/html/skin/

mysql -h $MYSQL_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < magento-sample-data-1.9.1.0/magento_sample_data_for_1.9.1.0.sql
