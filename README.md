# Skeleton Magento 1.9.X Docker Image for Development

This repo creates a Skeleton Docker Image for [Magento 1.9.x](http://magento.com/) Development.

#### Please note

> The primary goal of this repo is to create Docker images for Magento 1.9.x development and testing, especially for extensions and themes development.

## Magento versions

Version | Git branch | Tag name
--------| ---------- |---------
1.9.2.4 | master     | latest

## How to use

### Use Docker Compose

[Docker Compose](https://docs.docker.com/compose/) is the recommended way to run this image with MySQL database.

A inital `docker-compose.yml` can be found in this repo.

Use `docker-compose up -d` to start MySQL and Magento server.

## Magento sample data

Magento 1.9.X sample data is compressed version from [Vinai/compressed-magento-sample-data](https://github.com/Vinai/compressed-magento-sample-data).

## Magento installation script

The Magento installation script installs Magento without using web UI. This script requires certain environment variables to run:

Environment variable      | Description | Default value (used by Docker Compose - `env` file)
--------------------      | ----------- | ---------------------------
MYSQL_HOST                | MySQL host  | mysql
MYSQL_DATABASE            | MySQL db name for Magento | magento
MYSQL_USER                | MySQL username | magento
MYSQL_PASSWORD            | MySQL password | magento
MAGENTO_LOCALE            | Magento locale | en_GB
MAGENTO_TIMEZONE          | Magento timezone | Europe/Berlin
MAGENTO_DEFAULT_CURRENCY  | Magento default currency | EUR
MAGENTO_URL               | Magento base url | http://local.magento
MAGENTO_ADMIN_FIRSTNAME   | Magento admin firstname | Admin
MAGENTO_ADMIN_LASTNAME    | Magento admin lastname | MyStore
MAGENTO_ADMIN_EMAIL       | Magento admin email | admin@example.com
MAGENTO_ADMIN_USERNAME    | Magento admin username | admin
MAGENTO_ADMIN_PASSWORD    | Magento admin password | magentorocks1

You can just modify `env` file in the same directory of `docker-compose.yml` file to update those environment variables.

If you use default base url (http://local.magento) or other test url, you need to [modify your host file](http://www.howtogeek.com/howto/27350/beginner-geek-how-to-edit-your-hosts-file/) to map the host name to docker container. For Boot2Docker, use `boot2docker ip` to find the IP address.

**Important**: If you do not use the default `MAGENTO_URL` you must use a hostname that contains a dot within it (e.g `foo.bar`), otherwise the [Magento admin panel login won't work](http://magento.stackexchange.com/a/7773).

