FROM occitech/magento:php5.5-apache

ENV MAGENTO_VERSION 1.9.2.4

RUN apt-get update && apt-get install -y mysql-client-5.5 libxml2-dev

RUN docker-php-ext-install soap

RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN echo -n "\nxdebug.remote_enable=true\nxdebug.remote_connect_back=1\n" >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN cd /tmp && \
    curl https://codeload.github.com/OpenMage/magento-mirror/tar.gz/$MAGENTO_VERSION -o magento-$MAGENTO_VERSION.tar.gz

COPY ./bin/install-magento.sh /usr/local/bin/install-magento.sh
RUN chmod +x /usr/local/bin/install-magento.sh

RUN cd /tmp && \
    curl https://raw.githubusercontent.com/Vinai/compressed-magento-sample-data/1.9.1.0/compressed-no-mp3-magento-sample-data-1.9.1.0.tgz -o sample-data-1.9.1.0.tar.gz

COPY ./bin/install-sampledata.sh /usr/local/bin/install-sampledata.sh
RUN chmod +x /usr/local/bin/install-sampledata.sh

COPY ./bin/wait-for-it.sh /usr/local/bin/wait-for-mysql
RUN chmod +x /usr/local/bin/wait-for-mysql

ADD start.sh /start.sh
CMD /start.sh
