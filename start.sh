#!/bin/bash

if [[ -e /firstrun ]]; then

	echo >&2 "Not the first run so skipping initialization ..."

else 

	echo >&2 "Installing magento ..."

	touch /firstrun

	if [[ -e /var/www/html/.htaccess ]] ; then
	
		echo >&2 "Magento already installed!!!"
		
	else

		cd /tmp && tar xvf magento-$MAGENTO_VERSION.tar.gz && mv magento-mirror-$MAGENTO_VERSION/* magento-mirror-$MAGENTO_VERSION/.htaccess /var/www/html

		echo >&2 "Waiting for mysql server to start ..."
	
		wait-for-mysql -t 300 $MYSQL_HOST:3306

		install-sampledata.sh

		install-magento.sh

		chown -R $MAGENTO_USER:$MAGENTO_GROUP /var/www/html

	fi

fi

apache2-foreground

